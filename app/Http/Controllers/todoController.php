<?php

namespace App\Http\Controllers;

use App\Todo;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;

class todoController extends Controller
{
    /**
     * @var Todo
     */
    private $todo;

    function __construct(Todo $todo)
    {
        $this->todo = $todo;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $todo['newest'] = $this->todo->newest();
        $todo['uncompleted'] = $this->todo->uncompleted();
//        $todo['overdue'] = $this->todo->overdue();
//        $todo['upcoming'] = $this->todo->upcoming();
        $todo['completed'] = $this->todo->completed();
        return response()->json($todo);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [
            'title' => Input::get('title')
        ];

        Todo::create($data);

        return response()->json(['success' => true]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $todo = Todo::find($id);
        if($request->input('completed_date')){
            $todo->update(['completed_date' => Carbon::now()]);
            return response()->json();
        }

        $todo->update($request->all());

        return response()->json($todo);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Todo::destroy($id);

        return response()->json(['success' => true]);
    }
}
