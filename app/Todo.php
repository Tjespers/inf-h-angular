<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{
    protected $fillable = [
        'title',
        'completed_date'
    ];

    public function scopeNewest($query){
        return $query->orderBy('created_at', 'DESC')->limit(3)->get();
    }

    public function scopeCompleted($query)
    {
        return $query->where('completed_date', '!=', null)->get();
    }

    public function scopeOverdue($query){
        return $query->where('reminder_date', '<', Carbon::now())->get();
    }

    public function scopeUpcoming($query)
    {
        return $query->where('reminder_date', '>', Carbon::now())->get();
    }

    public function scopeUncompleted($query)
    {
        return $query->where('completed_date', null)->get();
    }

}
