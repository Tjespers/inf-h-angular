angular.module('todoService',[])
    .factory('Todo', function($http) {

        return {
            get : function(){
                return $http.get('api/todo');
            },

            save : function(todoData) {
                return $http({
                    method: 'POST',
                    url:    'api/todo',
                    headers: {'Content-Type' : 'application/x-www-form-urlencoded'},
                    data: $.param(todoData)
                });
            },

            destroy : function(id){
                return $http.delete('api/todo/' + id);
            },

            update : function(todoData){
                return $http({
                    method: 'PATCH',
                    url: 'api/todo/'+todoData.id,
                    headers: {'Content-Type' : 'application/x-www-form-urlencoded'},
                    data: $.param(todoData)
                });
            },

            complete : function(id) {
                return $http({
                    method: 'PATCH',
                    url: 'api/todo/'+id,
                    headers: {'Content-Type' : 'application/x-www-form-urlencoded'},
                    data: $.param({completed_date: new Date().toISOString().slice(0, 19).replace('T', ' ')})
                });
            }
        }
    });