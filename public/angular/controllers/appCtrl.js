angular.module('appCtrl', ['ui.bootstrap'])
    .controller('appController', function ($scope, $http, $filter, $modal, $log,Todo) {

        $scope.todoData = {};

        $scope.loading = true;

        Todo.get()
            .success(function (data) {
                $scope.todos = data;
                $scope.loading = false;
            });

        $scope.submitTodo = function () {
            $scope.loading = true;

            Todo.save($scope.todoData)
                .success(function (data) {
                    $scope.todoData = {};
                    Todo.get().success(function(getData){
                        $scope.todos = getData;
                        $scope.loading = false;
                    });
                })
                .error(function(data){
                    console.log(data);
                });
        };

        $scope.deleteTodo = function(id) {
            $scope.loading = true;

            Todo.destroy(id)
                .success(function(data){
                    Todo.get()
                        .success(function(getData){
                            $scope.todos = getData;
                            $scope.loading = false;
                        });
                });
        };

        $scope.editTodo = function (id, type) {
            $todo = $filter('getById')($scope.todos[type], id);
            $scope.open(type, id);
        };

        $scope.completeTodo = function(id) {
            $scope.loading = true;

            Todo.complete(id).success(function(data){
                Todo.get()
                    .success(function(getData){
                        $scope.todos = getData;
                        $scope.loading = false;
                    })
            })
        }

        $scope.open = function (type, todoId, size) {
            var modalInstance = $modal.open({
                templateUrl: 'myModalContent.html',
                controller: 'ModalInstanceCtrl',
                size: size,
                resolve: {
                    todo: function () {
                        return $filter('getById')($scope.todos[type], todoId);
                    }

                }
            });

            modalInstance.result.then(function (updatedTodo) {
                Todo.update(updatedTodo)
                    .success(function(data){
                        $scope.loading = true;
                        Todo.get()
                            .success(function(getData){
                                $scope.todos = getData;
                                $scope.loading = false;
                            });
                    });
            }, function () {});
        };

    })
   .controller('ModalInstanceCtrl', function ($scope, $modalInstance, todo) {
    $scope.title = todo.title;
    $scope.id = todo.id;

    $scope.ok = function () {
        $modalInstance.close({id: $scope.id, title: $scope.title});
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});