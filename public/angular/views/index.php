<!doctype html> <html lang="en"> <head> <meta charset="UTF-8"> <title>Laravel and Angular Comment Syste</title>

    <!-- CSS -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css"> <!-- load bootstrap via cdn -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"> <!-- load fontawesome -->
    <style>
        body        { padding-top:30px; }
        form        { padding-bottom:20px; }
        .todo       { margin-bottom:1px; }
        .odd        { background-color: #eee;}
        .btnBox     { display: flex; width: 100%; justify-content: space-between}
    </style>

    <!-- JS -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.8/angular.min.js"></script> <!-- load angular -->
    <script src="http://angular-ui.github.com/bootstrap/ui-bootstrap-tpls-0.12.0.js"></script>

    <!-- ANGULAR -->
    <!-- all angular resources will be loaded from the /public folder -->
    <script src="angular/controllers/appCtrl.js"></script> <!-- load our controller -->
    <script src="angular/services/todoService.js"></script> <!-- load our service -->
    <script src="angular/app.js"></script> <!-- load our application -->


</head>
<!-- declare our angular app and controller -->
<body class="container" ng-app="todoApp" ng-controller="appController">

    <!-- PAGE TITLE =============================================== -->
    <div class="page-header">
        <h2>Laravel & Angular Single Page Application</h2>
        <h4>Todo System</h4>
        <h5>Tim Jespers (10013709)</h5>
    </div>

    <div class="row">
        <div class="col-lg col-lg-6">
            <div class="page-header">
                <h2>Info</h2>
            </div>
            <div class="jumbotron">
                Met deze sinlge page app kan eenvoudig een Todo list worden aangemaakt, uitgelezen en onderhouden. Deze applicatie is gemaakt door Tim Jespers, als opdracht voor INF-H Angular, applicatie bouwen.
            </div>
        </div>
        <div class="col-lg col-lg-6">

            <div class="page-header">
                <h2>Nieuwe ToDo aanmaken</h2>
            </div>
            <!-- NEW COMMENT FORM =============================================== -->
            <form ng-submit="submitTodo()"> <!-- ng-submit will disable the default form action and use our function -->

                <!-- AUTHOR -->
                <div class="form-group">
                    <textarea class="form-control" name="title" ng-model="todoData.title" placeholder="Omschrijving" ></textarea>
                </div>

                <!-- COMMENT TEXT -->
                <!-- <div class="form-group">
                     <input type="text" class="form-control input-lg" name="comment" ng-model="commentData.text" placeholder="Say what you have to say">
                 </div>-->

                <!-- SUBMIT BUTTON -->
                <div class="form-group text-right">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-lg fa-fw fa-floppy-o"></i> Opslaan</button>
                </div>
            </form>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-4">
            <div class="page-header">
                <h2>Nieuwste ToDo's</h2>
            </div>
            <p class="text-center" ng-show="loading"><span class="fa fa-spinner fa-5x fa-spin"></span></p>
            <p class="text-center" ng-hide="loading || todos.newest.length > 0"> Geen Todo's in deze category</p>
            <div class="todo" ng-hide="loading" ng-repeat="todo in todos.newest">
                <div class="todo" ng-if="$even">
                    <h3>Todo #{{ todo.id }} <small> created {{todo.created_at}}</small></h3>
                    <small ng-if="todo.created_at != todo.updated_at">last updated: {{todo.updated_at}}</small>
                    <p>{{ todo.title }}</p>
                    </div>
                <div class="todo odd" ng-if="$odd">
                    <h3>Todo #{{ todo.id }} <small> created {{todo.created_at}}</small></h3>
                    <small ng-if="todo.created_at != todo.updated_at">last updated: {{todo.updated_at}}</small>
                    <p>{{ todo.title }}</p>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header">
                <h2>Openstaande ToDo's</h2>
            </div>
            <p class="text-center" ng-show="loading"><span class="fa fa-spinner fa-5x fa-spin"></span></p>
            <p class="text-center" ng-hide="loading || todos.uncompleted.length > 0"> Geen Todo's in deze category</p>
            <div class="todo" ng-hide="loading" ng-repeat="todo in todos.uncompleted">
                <div class="todo" ng-if="$even">
                    <h3>Todo #{{ todo.id }} <small> created {{todo.created_at}}</small></h3>
                    <small ng-if="todo.created_at != todo.updated_at">last updated: {{todo.updated_at}}</small>
                    <p>{{ todo.title }}</p>
                    <div class="btnBox">
                        <button href="#" ng-click="editTodo(todo.id, 'uncompleted')" class="btn btn-info pull-right"><i class="fa fa-lg fa-fw fa-pencil"></i>Edit</button>
                        <button href="#" ng-click="completeTodo(todo.id)" class="btn btn-success pull-right"><i class="fa fa-lg fa-fw fa-check"></i>Complete</button>
                        <button href="#" ng-click="deleteTodo(todo.id)" class="btn btn-danger pull-right"><i class="fa fa-trash fa-lg fa-fw"></i>Delete</button>
                    </div>
                </div>
                <div class="todo odd" ng-if="$odd">
                    <h3>Todo #{{ todo.id }} <small> created {{todo.created_at}}</small></h3>
                    <small ng-if="todo.created_at != todo.updated_at">last updated: {{todo.updated_at}}</small>
                    <p>{{ todo.title }}</p>
                    <div class="btnBox">
                        <button href="#" ng-click="editTodo(todo.id, 'uncompleted')" class="btn btn-info pull-right"><i class="fa fa-lg fa-fw fa-pencil"></i>Edit</button>
                        <button href="#" ng-click="completeTodo(todo.id)" class="btn btn-success pull-right"><i class="fa fa-lg fa-fw fa-check"></i>Complete</button>
                        <button href="#" ng-click="deleteTodo(todo.id)" class="btn btn-danger pull-right"><i class="fa fa-trash fa-lg fa-fw"></i>Delete</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="page-header">
                <h2>Afgeronde ToDo's</h2>
            </div>
            <p class="text-center" ng-show="loading"><span class="fa fa-spinner fa-5x fa-spin"></span></p>
            <p class="text-center" ng-hide="loading || todos.completed.length > 0"> Geen Todo's in deze category</p>
            <div ng-hide="loading" ng-repeat="todo in todos.completed">
                <div class="todo" ng-if="$even">
                    <h3>Todo #{{ todo.id }} <small> created {{todo.created_at}}</small></h3>
                    <small ng-if="todo.created_at != todo.updated_at">last updated: {{todo.updated_at}}</small>
                    <p>{{ todo.title }}</p>
                    <button href="#" ng-click="deleteTodo(todo.id)" class="btn btn-danger pull-right"><i class="fa fa-trash fa-lg fa-fw"></i> Delete</button>
                    <div class="clearfix"></div>
                </div>
                <div class="todo odd" ng-if="$odd">
                    <h3>Todo #{{ todo.id }} <small> created {{todo.created_at}}</small></h3>
                    <small ng-if="todo.created_at != todo.updated_at">last updated: {{todo.updated_at}}</small>
                    <p>{{ todo.title }}</p>
                    <button href="#" ng-click="deleteTodo(todo.id)" class="btn btn-danger pull-right"><i class="fa fa-trash fa-lg fa-fw"></i>Delete</button>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>

    <div>
        <script type="text/ng-template" id="myModalContent.html">
            <div class="modal-header">
                <h3 class="modal-title">Todo item wijzigen</h3>
            </div>
            <div class="modal-body">
                Voer nieuwe todo text in:
                <textarea class="form-control" ng-model="title"/>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" type="button" ng-click="ok()"><i class="fa fa-lg fa-fw fa-floppy-o"></i> Opslaan</button>
                <button class="btn btn-warning" type="button" ng-click="cancel()"><i class="fa fa-fw fa-lg fa-times"></i> Sluiten</button>
            </div>
        </script>
    </div>
</body>
</html>