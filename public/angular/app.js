var todoApp = angular.module('todoApp', ['appCtrl', 'todoService', 'ui.bootstrap']);

todoApp.filter('getById', function() {
    return function(input, id) {
        var i=0, len=input.length;
        for (; i<len; i++) {
            if (+input[i].id == +id) {
                return input[i];
            }
        }
        return null;
    }
});